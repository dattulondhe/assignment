import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PicofthedayComponent } from './picoftheday.component';
import { HttpClientModule } from '@angular/common/http';

describe('PicofthedayComponent', () => {
  let component: PicofthedayComponent;
  let fixture: ComponentFixture<PicofthedayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PicofthedayComponent],
      imports: [HttpClientModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PicofthedayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  // checking getPicofTheDay method
  it('getPicofTheDay should there', () => {
    expect(component.getPicofTheDay).toBeTruthy();
  });

});
