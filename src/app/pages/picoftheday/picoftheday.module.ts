import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PicofthedayRoutingModule } from './picoftheday-routing.module';
import { PicofthedayComponent } from './picoftheday.component';


@NgModule({
  declarations: [PicofthedayComponent],
  imports: [
    CommonModule,
    PicofthedayRoutingModule
  ]
})
export class PicofthedayModule { }
