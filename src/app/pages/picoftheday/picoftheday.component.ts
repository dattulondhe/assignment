import { Component, OnInit } from '@angular/core';
import { WebService } from 'src/app/service/web.service';

@Component({
  selector: 'app-picoftheday',
  templateUrl: './picoftheday.component.html',
  styleUrls: ['./picoftheday.component.scss']
})
export class PicofthedayComponent implements OnInit {
  // tslint:disable-next-line: max-line-length
  pickOfTheDay: object = {};
  constructor(private ws: WebService) { }
  ngOnInit() {
    this.getPicofTheDay();
  }

  getPicofTheDay() {
    this.ws.getPicofTheDay().subscribe(res => {
      console.log(res);
      if (res.callback == 'success') {
        this.pickOfTheDay = res.contextWrites.to;
      } else {

      }
    });
  }

}
