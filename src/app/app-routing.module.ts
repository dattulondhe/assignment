import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  { path: 'picoftheday', loadChildren: () => import('./pages/picoftheday/picoftheday.module').then(m => m.PicofthedayModule) , },
  { path: 'asteroidStats', loadChildren: () => import('./pages/asteroid-stats/asteroid-stats.module').then(m => m.AsteroidStatsModule) },
  
  { path: '', redirectTo: 'picoftheday', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
