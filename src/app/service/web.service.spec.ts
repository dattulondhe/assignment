import { TestBed } from '@angular/core/testing';

import { WebService } from './web.service';
import { HttpClientModule } from '@angular/common/http';

describe('WebService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientModule]
  }));

  it('should be created', () => {
    const service: WebService = TestBed.get(WebService);
    expect(service).toBeTruthy();
  });
});
